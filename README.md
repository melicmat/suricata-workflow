# Vývoj Suricata pravidel na virtuálním stroji pomocí Vagrant a VS Code pro potřeby rozpoznání EMOTET mallware #
Tento návod poslouží každému, kdo chce v předmětu BI-HAM (Hardwarově
akcelerované monitorování síťového provozu) vyzkoušet tvorbu vlastních
pravidel pro Suricata IDS bez nutnosti instalace na svůj osobní stroj a
image.

S výhodou využijeme nástroj ***Vagrant*** pro snadné vytváření a správu
virtuálních strojů s profilem vytvořeným specificky pro tento předmět.
(Autorem tohoto profilu Ing. Tomáš Čejka, PhD - aktuální garant a
přednášející BI-HAM). Dále se nám bude hodit populární editor
***Visual Studio Code*** z důvodu jeho rozšiřitelnosti zásuvnými moduly (pluginy), které zde také využijeme.

Využijeme zejména pluginy:
* Pro syntax highlithing Suricata pravidel
* Pro SSH připojení pro potřeby remote vývoje

## Instalace prerekvizit ##

Takto jsme tedy vytvořili konfiguraci a nyní se už můžeme připojit pomocí příkazu v VS Code příkazovém řádku.
Předpokládáme, že čtenář má ve svém OS již nainstalovaný editor
***Visual Studio Code*** a nástroj ***Vagrant*** který je v BI-HAM velmi
dobré používat vzhledem k existenci vagrantfilů které pro tento předmět
vznikly.

### Instalace pluginů pro VS Code ###
Prvně nainstalujme pluginy pro VS code. Instalujeme:
1. [Suricata highlight](https://marketplace.visualstudio.com/items?itemName=dgenzer.suricata-highlight-vscode)
2. [Remote - SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)

Po instalaci těchto pluginů pokračujeme přípravou virtuálního stroje.

### Instalace virtuálního stroje ###
Virtuální stroj je založen na výše zmíněné image
připravené od Tomáše, avšak v době tvorby tohoto
manuálu není Suricata IDS běžnou součástí této image.
Je tedy potřeba ji doinstalovat a zde je velmi žádoucí
pro automatickou instalaci při vytvoření virtuálního
stroje využít toho, že do ***Vagrantfile*** můžeme
zahrnout provisioning sekci, kam můžeme vložit kus kódu
v shellu který se pak automaticky provede.

Vagrantfile pro takový virtuální stroj tedy bude vypadat takto:

	Vagrant.configure("2") do |config|
		$script = <<-SCRIPT
		yes "y" | sudo yum install epel-release
		yes "y" | sudo yum install suricata
		git clone https://github.com/pan-unit42/wireshark-tutorial-Emotet-traffic.git pcap-files
		mkdir Rules
		SCRIPT

		config.vm.box = "cejkat/biham2022-minimal"
		config.vm.box_version = "1.0.0"
		config.ssh.username = "biham"
		config.ssh.password = "biham"
		config.vm.provision "shell", inline: $script, privileged: false
	end

Takto tedy říkáme, že při vykonání `vagrant provision` se provede instalce ***Suricaty*** naklonují se záchyty ***Emotet Mallware*** a vytvoří se adresář pro pravidla, ve kterém pak můžeme vytvářet pravidla.

Tedy nejdříve v námi zvoleném adresáři ve kterém máme zmíněný Vagrantfile spustíme `vagrant up` a po úspěšné instalaci stroje poté `vagrant provision` a tento příkaz provede samotný inline shell skript z Vagrantfile.

Po vykonání předchozích dvou příkazů bychom měli po přípojení ke stroji pomocí `vagrant ssh` a následném vykonání `ls -l` vidět takový výstup:

	[biham@localhost ~]$ ls -l
	total 60936
	drwxrwxr-x 3 biham biham     4096 29. čen 11.21 pcap-files
	drwxrwxr-x 2 biham biham     4096 29. čen 11.21 Rules
	-rw-r--r-- 1 biham biham 62388224 31. bře  2022 VBoxGuestAdditions_6.1.32.iso

Tedy vidíme, že se pravděpodobně vše podařilo a můžeme pokračovat.

## Připojení z VS Code ##
Nyní se již můžeme připojit z VS Code. Abychom mohli pravidla tvořit přímo ve virtuálním stroji.
Pro získání informací pro SSH připojeni musíme vykonat `vagrant ssh-config` a výstup by měl být následující:

	Host default
		HostName 127.0.0.1
		User biham
		Port 2222
		UserKnownHostsFile /dev/null
		StrictHostKeyChecking no
		PasswordAuthentication no
		IdentityFile /home/melimat/FIT/BI-HAM/Suricata-guide/.vagrant/machines/default/virtualbox/private_key
		IdentitiesOnly yes
		LogLevel FATAL

Takto získanou konfiguraci přidáme do SSH konfiguračního souboru. Ten může být v domovském adresáři ve skrytém podadresáři `.ssh` konkrétně do souboru `.ssh/config` nicméně zde je možná odlišnost v umístění.


Pro připojení použijeme příkazový řádek v VS Code (vyvolání `Ctrl + P`) a napíšeme:
`Remote-SSH: Connect to Host` a toto vykonáme.
Měli bychom vidět následující nabídku:

![SSH-vyber](Figures/ssh-choose.png)

A zvolíme podle předchozího bodu host jménem default. (Dle výstupu `vagrant ssh-config`)

Tím by se měl VS Code přes SSH připojit k našemu virtuálnímu stroji. Při
prvním připojení si VS Code nainstaluje poměrně jednoduchý server na náš 
virtuální stroj, ten je potřebný pro to, aby nám VS Code fungovaly 
všechny funkce, které normálně fungují při editaci souborů na lokálním 
stroji. Toto se děje samo na pozadí a příliš se tím nemusíme zabývat. 
Při opětovném připojování se samotná instalace již pochopitelně 
neopakuje.

Při opakovaném připojování pak už vždy jen vybereme správný `host`, žádnou editaci konfigurace pak již dělat samozřejmě nemusíme.

## Vlastní postup úpravy pravidel ##
To že se připojení zdařilo, můžeme vidět v levém spodním rohu.

![SSH úspěšné připojení](Figures/ssh-success.png)

A můžeme tedy otevřít adresář, ve kterém budeme chtít tvořit pravidla.

Otevřeme tedy pomocí `Open Folder` tlačítka domovský adresář uživatele `biham` na virtuálním stroji.

![Tlacitko](Figures/ssh-open-folder.png)

A po stisknutí tlačítka stačí již jen potvrdit.

![Folder](Figures/dir.png)

A měli bychom vidět v levém sloupci určenému zobrazování adresářové 
struktury a souborů následující strukturu, kterou jsme vytvořili pomocí 
zmíněného provisioningu.

![Struktura](Figures/structure.png)

Na začátku jsme zmiňovali, že bychom chtěli použít adresář `Rules` jako 
místo kde budeme editovat soubory s pravidly. Tedy stačí už jen obyčejně 
soubory v daném adresáři vytvořit a editovat.

Hodí se mít vedle VS Code otevřený terminál s SSH do virtuálního stroje aby bylo možné rovnou spouštět Suricatu nad naklonovanými záchyty s vytvářenými pravidly pro urychlení testování.

## Závěrem ##
Tento návod rozhodně nepokrývá dostatečně ani nástroj ***Vagrant*** a od čtenáře se také čeká základní znalost ***VS Code***

Rozhodně se nejedná o jediný postup, jak pravidla vyvíjet, ale - poznámka autora - mně se velmi osvědčil. Možná se jedná o přílišný overengineering. Ale tato cesta mně osobně přišla poměrně komfortní.

Autor: Matěj Melichna (melicmat@fit.cvut.cz)